package droneSim;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.swing.JFileChooser;

/**
 * Simple program to show arena with multiple drones
 * 
 * @author shsmchlr
 *
 */

public class DroneInterface {

	private Scanner s; // scanner used for input from user
	private DroneArena myArena; // arena in which drones are shown
	private boolean canPass = false;
	int arenaSizeX;
	int arenaSizeY;

	/**
	 * constructor for DroneInterface sets up scanner used for input and the
	 * arena then has main loop allowing user to enter commands
	 */
	public DroneInterface() {
		s = new Scanner(System.in); // set up scanner for user input
		char ch = ' ';
		do {
			System.out.print("create (N)ew Arena, (L)oad Arena or e(X)it > ");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
			case 'n':
			case 'N':
				Scanner arenaSize = new Scanner(System.in);
				System.out.println("Enter length of arena: "); //User input for arena length
				arenaSizeX = arenaSize.nextInt();

				System.out.println("Enter height of arena: "); //User input for arena height
				arenaSizeY = arenaSize.nextInt();

				myArena = new DroneArena(arenaSizeX, arenaSizeY); // create arena
				canPass = true;
				break;

			case 'l':
			case 'L':
				//Open a dialogue for the user to select and open (load) a txt file.
				JFileChooser choice = new JFileChooser();
				int option = choice.showOpenDialog(null);
				if (option == JFileChooser.APPROVE_OPTION) {
					File file = choice.getSelectedFile();
					String path = file.getAbsolutePath();
					try (Scanner scan = new Scanner(new BufferedReader(new FileReader(file)))) {
						BufferedReader reader = new BufferedReader(new FileReader(file));
						int lines = 0;
						while (reader.readLine() != null) {
							lines++;
						}
						reader.close();
						String text = Files.readAllLines(Paths.get(path)).get(0); //below code only works on the first line, i.e. the arena size
						String[] parts = text.split(",");
						String part1 = parts[0];
						String part2 = parts[1];

						arenaSizeX = Integer.parseInt(part1);
						arenaSizeY = Integer.parseInt(part2);

						myArena = new DroneArena(arenaSizeX, arenaSizeY); // create arena

						for (int i = 1; i < lines; i++) { //starting from the second row, add every drone to the drones arraylist
							String drone = Files.readAllLines(Paths.get(path)).get(i);
							DroneArena.drones.add(drone);
						}
					} catch (IOException e) {
						System.err.println("Error: " + e.getMessage());
						e.printStackTrace();
					}
				}
				canPass = true;
				break;

			case 'x':
			case 'X':
				s.close(); // close scanner
				canPass = false;
				System.out.println("Exiting the program");
				System.exit(0);
				break;

			}
		} while (!canPass);

		do {
			System.out.print(
					"Enter (A)dd drone, get (I)nformation, (D)isplay canvas, (M)ove all drones, (S)ave Arena or e(X)it > ");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
			case 'A':
			case 'a':
				myArena.addDrone(); // add a new drone to arena
				break;
			case 'I':
			case 'i':
				System.out.print(myArena.toString());
				break;
			case 'd':
			case 'D':
				doDisplay();

				break;
			case 'm':
			case 'M':
				DroneArena.moveAllDrones();
				break;
			case 'b':
			case 'B':
				System.out.print(myArena.toString());
				DroneArena.moveAllDronesTenTimes();
				break;

			case 's':
			case 'S':
				//User selects where to save the file and what to name it.
				JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();

					PrintWriter out;
					try {
						out = new PrintWriter(new BufferedWriter(new FileWriter(file)));

						out.print(arenaSizeX + "," + arenaSizeY); //Write the arena size on the first line

						for (String drone : DroneArena.drones) { //Write each drone in the arraylist on a new line
							out.print("\n");
							out.print(drone);
						}
						out.close();
					} catch (IOException e) {

						e.printStackTrace();
					}
				}

				break;
			case 'x':
				ch = 'X'; // when X detected program ends
				break;
			}
		} while (ch != 'X'); // test if end

		s.close(); // close scanner
	}

	public static void main(String[] args) {
		DroneInterface r = new DroneInterface(); // just call the interface
	}

	static void doDisplay() {
		// determine the arena size
		int x = DroneArena.x;
		int y = DroneArena.y;
		// hence create a suitable sized ConsoleCanvas object
		ConsoleCanvas cv = new ConsoleCanvas(y, x);
		// call showDrones suitably
		DroneArena.showDrones(cv);
		// then use the ConsoleCanvas.toString method
		System.out.println(cv.toString());

	}

}
