package droneSim;

public class ConsoleCanvas {

	public static String[][] canvas;

	public ConsoleCanvas(int x, int y) {
		x += 2; // +2 for 1 top border and 1 bottom border
		y += 2; // +2 for 1 left-side border and 1 right-side border
		canvas = new String[x][y];
		for (int j = 0; j <= y - 1; j++) { // row iteration

			for (int i = 0; i <= x - 1; i++) { // column iteration

				if (j == 0 || j == y - 1 || i == 0 || i == x - 1) {

					canvas[i][j] = "#"; //print a # around the arena to show the border;

				} else {

					canvas[i][j] = " ";
				}
			}

		}
	}

	public static void main(String[] args) {
		// ConsoleCanvas c = new ConsoleCanvas (10, 5); // create a canvas
		// c.showIt(4,3,'D');
		// c.showIt(3,4,'F');// add a Drone at 4,3
		// System.out.println(c.toString()); // display result
	}

	public String toString() {
		String StringBuilder = "";
		for (int r = 0; r < canvas.length; r++) { // for loop for row iteration.
			for (int z = 0; z < canvas[r].length; z++) { // for loop for column iteration.
				StringBuilder += canvas[r][z] + " ";
				
			}
			StringBuilder += "\n"; // using this for new line to print array in matrix format.
		}
		return StringBuilder;

	}

	void showIt(int i, int j, char c) {
		String letter = Character.toString(c);
		canvas[j][i] = letter;

	}

}
