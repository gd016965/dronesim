package droneSim;

import java.util.Random; //import the random module
import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

public class DroneArena implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6478342463652727456L;
	// private static final Drone Drone = null;
	static int x; // initialisation of x coordinate
	static int y; // initialisation of y coordinate
	static char[][] Arena; // initialisation of 2D array, i.e. Arena
	public static int xRan; // initialisation of random x coordinate
	public static int yRan; // initialisation of random y coordinate
	public static CopyOnWriteArrayList<String> drones = new CopyOnWriteArrayList<String>();
	static int i = 0; // ArrayList index pointer initialisation
	static int id = 0;
	static Direction direction;
	static Direction updatedDirection;
	static Drone d = new Drone(x, y, direction);
	static int accX;
	static int accY;
	public static Direction accDir;

	public static String str;
	static int xCoord;
	static int yCoord;

	static String[] coordsList;

	public DroneArena(int xLen, int yLen) {

		x = xLen;
		y = yLen;

		Arena = new char[xLen][yLen];
	}

	public static String genRandomCoords() {
		String coords = "";
		Random randomGenerator;
		randomGenerator = new Random();
		xRan = randomGenerator.nextInt(x) + 1; // +1 to ensure drone is not assigned border position
		coords += xRan + ",";
		randomGenerator = new Random();
		yRan = randomGenerator.nextInt(y) + 1; // +1 to ensure drone is not assigned border position
		coords += yRan;

		return coords;

	}

	public void addDrone() {
		boolean isHereBool;
		if (drones.size() < (x * y)) {

			do {
				str = genRandomCoords();
				xCoord = Integer.parseInt(str.substring(0, str.indexOf(","))); //get x coord
				yCoord = Integer.parseInt(str.substring(str.indexOf(",") + 1, str.length())); //get y coord

				direction = Direction.getCurrentDirection();
				isHereBool = Drone.isHere(xCoord, yCoord);

			} while (isHereBool == true);

			drones.add(i, xCoord + "," + yCoord + "," + direction); 

			i++;

		} else {
			//arena is full
			System.out.println("Arena full");
		}

	}
	

	public static Drone getDroneAt(int x, int y) {
		if (drones.contains(x + "," + y)) {
			System.out.println("Drone at " + x + "," + y);
		}

		return null;
	}

	public static void showDrones(ConsoleCanvas c) {
		for (String drone : drones) {
			String[] coords = drone.split(",");
			accX = Integer.parseInt(coords[0]); // Split element into separate coordinates AKA Actual X coordinate
			accY = Integer.parseInt(coords[1]);// Split element into separate coordinates AKA Actual Y coordinate
			Drone obj = new Drone(accX, accY, direction);
			obj.displayDrone(c);

		}
	}

	public String toString() {
		String listOfDrones = "";

		for (String drone : drones) {
			id = drones.indexOf(drone);
			String accDir = drone.split(",")[2]; // String value of direction from the element in the arrayList
			String[] coords = drone.split(",");
			accX = Integer.parseInt(coords[0]);
			accY = Integer.parseInt(coords[1]);
			listOfDrones += "Drone " + id + " is at (" + accX + ", " + accY + "), in the direction of " + accDir + "\n";

		}
		return listOfDrones;
	}

	public static void main(String[] args) {
		DroneArena a = new DroneArena(2, 4);
		a.addDrone();
		a.addDrone();
		a.addDrone();
		a.addDrone();
		a.addDrone();
		a.addDrone();
		// canMoveHere(5,5);
		// System.out.println(drones);
		System.out.println(a.toString());
	}

	public static boolean canMoveHere(int newX, int newY) {

		if (newY < 1 || newY > y || newX < 1 || newX > x) { //check to see if position is inside or outside of the arena

			return false;

		}

		return true;

	}

	public static void updateDrone(int id2, Direction direction, int newX, int newY) {
		//updates the drone by removing and readding it with new values. Could use .set() instead?
		updatedDirection = direction;
		DroneArena.direction = updatedDirection;
		drones.remove(id2);
		drones.add(id2, newX + "," + newY + "," + updatedDirection);
		

	}

	public static void moveAllDrones() {
		String strOfCurrentDirection;
		Direction currentDirection;

		for (String drone : drones) {
			id = drones.indexOf(drone);
			String[] coords = drone.split(",");
			accX = Integer.parseInt(coords[0]);// get x coordinate
			accY = Integer.parseInt(coords[1]);// get y coordinate

			String droneWithCurrentID = drones.get(id); // direction in string
			strOfCurrentDirection = droneWithCurrentID.split(",")[2];
			currentDirection = Direction.valueOf(strOfCurrentDirection); // convert string to direction

			Drone.tryToMove(id, currentDirection, accX, accY);
			System.out.println("Drone " + id + " is at (" + accX + ", " + accY + "), in the direction of "
					+ strOfCurrentDirection + "\n");
		}

		DroneInterface.doDisplay();

	}

	public static void wait2Seconds() {
		try {
			TimeUnit.SECONDS.sleep(2); //sleep for 200ms as requested
			DroneInterface.doDisplay();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public static void moveAllDronesTenTimes() {
		for (int i = 0; i <= 10; i++) {
			moveAllDrones();
			wait2Seconds();
		}
	}

}
