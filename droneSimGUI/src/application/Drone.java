package application;

import application.Direction;
import application.Drone;
import application.DroneArena;

public class Drone {

	public static int id = 0;
	static int count = 0;
	public static int xPos;
	public static int yPos;
	public static Direction direction;
	public static Direction nextDirection;
	String str;
	
	/**
	 * 
	 * @param x | current x position
	 * @param y | current y position
	 * @param direction | current direction
	 */
	public Drone(int x, int y, Direction direction) {
		id = count++;
		xPos = x;
		yPos = y;
		Drone.direction = DroneArena.direction;
	}
	
	/**
	 * This method attempts to move a drone. It achieves this by giving it a test. If it passes each test the drone is allowed to move.
	 * The first test is if the drone can move to the next location. The second test is if there is already a drone there.
	 * If it passes these, the update drone method is called which will ultimately move the drone.
	 * @param id2 | drones ID
	 * @param accDir | The current up-to-date direction
	 * @param x1 | the current up-to-date x position
	 * @param y1 | the current up-to-date y position
	 */

	public static void tryToMove(int id2, Direction accDir, int x1, int y1) {
		xPos = x1;
		yPos = y1;
		
		switch (accDir) {

		case NORTH:
			yPos -= 1; //move north by 1
			if (DroneArena.canMoveHere(xPos, yPos) && Drone.isHere(xPos, yPos) == false) {
				DroneArena.yRan = yPos;

			} else {
				yPos += 1; //retain current position
				accDir = Direction.nextDirection();
			}
			DroneArena.updateDrone(id2, accDir, xPos, yPos);
			break;
		case EAST:

			xPos += 1; //move east by 1
			if (DroneArena.canMoveHere(xPos, yPos) && Drone.isHere(xPos, yPos) == false) {

				DroneArena.xRan = xPos;

			} else {
				xPos -= 1; //retain current position
				accDir = Direction.nextDirection();
			}
			DroneArena.updateDrone(id2, accDir, xPos, yPos);
			break;
		case SOUTH:

			yPos += 1; //move south by 1
			if (DroneArena.canMoveHere(xPos, yPos) && Drone.isHere(xPos, yPos) == false) {
				DroneArena.yRan = yPos;

			} else {
				yPos -= 1; //retain current position
				accDir = Direction.nextDirection();
			}
			DroneArena.updateDrone(id2, accDir, xPos, yPos);
			break;
		case WEST:

			xPos -= 1; //move west by 1
			if (DroneArena.canMoveHere(xPos, yPos) && Drone.isHere(xPos, yPos) == false) {

				DroneArena.xRan = xPos;

			} else {
				xPos += 1; //retain current position
				accDir = Direction.nextDirection();
			}
			DroneArena.updateDrone(id2, accDir, xPos, yPos);
			break;

		}
	}

	public String toString() {
		//System.out.println(Drone.direction);
		str += "Drone " + count + " is at (" + xPos + ", " + yPos + "), in the direction of " + Drone.direction + "\n";
		return str;
	}
	
	/**
	 * 
	 * @param sx | the x coordinate the method should test
	 * @param sy | the y coordinate the method should test
	 * @return true or false depending on if there is a drone in the location or not.
	 */
	public static boolean isHere(int sx, int sy) {
		
		for (String drone : DroneArena.drones) {

			if (drone.startsWith(sx + "," + sy)) { //if there is a drone in the desired position

				return true;
			}
		}

		return false;

	}
}