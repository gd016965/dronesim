package application;
	
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.CopyOnWriteArrayList;
import application.DroneArena;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseEvent; 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class extends Application allowing the JavaFX GUI to be executed and shown.
 * @author JordanSavage
 * @version 1.0
 */
public class DroneInterface extends Application{
	
	Button droneArenaButton, returnToMenuButton, addDrone, fileButton, helpButton;
	static Button startAnimation;
	Button pauseAnimation;
	Stage window;
	Scene mainMenu, droneArena;
	static DroneArena d = new DroneArena(0, 0);
	static Pane layout2 = new Pane();
	
	ToolBar toolBar = new ToolBar();
	public static int xPos;
	public static int yPos;
	boolean pausePressed = true;
	int newArenaSizeX = 0;
	int newArenaSizeY = 0;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			window = primaryStage;
			
			//buttons and text for help screen
			Label menuText = new Label("Drone Simulator GUI Help \n To create a new arena: go to file->new arena. "
					+ "Here you will be able to create a new arena.\n To load an arena: go to file->load arena and "
					+ "select the arena file.");
			droneArenaButton = new Button("Return to Drone Arena");
			droneArenaButton.setLayoutX(0);
			droneArenaButton.setLayoutY(0);
			
			droneArenaButton.setOnAction(e -> window.setScene(droneArena)); //When the button Drone Arena is pressed, the scene changes
			menuText.setLayoutX(1000);
			menuText.setTextAlignment(TextAlignment.CENTER);
			Label createArena = new Label("Create or Load an Arena by going to 'File'");
			createArena.setVisible(true);
			createArena.setLayoutX(350);
			createArena.setLayoutY(500);
			
			Label droneInfo = new Label(d.toString()); //toString() returns information about the drone, in this case - d.
			droneInfo.setVisible(false); //This information should not be visible at the beginning because an arena and drones do not exist.
			droneInfo.setLayoutX(670);
			droneInfo.setLayoutY(20);
			
			/**
			 * An instance of an Animation Timer is created. When this animation timer is ran, 
			 * every drone visible and in the list will be moved and shown on the scene
			 * Information about said drones will be displayed too.
			 */
			AnimationTimer timer = new AnimationTimer(){
				public void handle(long currentNanoTime){
					DroneArena.moveAllDrones();
					droneInfo.setText(d.toString());
				}
			};
			
			//layout1 aka Help screen
			VBox layout1 = new VBox(10);
			layout1.getChildren().addAll(menuText, droneArenaButton, toolBar);
			mainMenu = new Scene(layout1, 1000, 1000);
			
			
			helpButton = new Button("Help");
			helpButton.setOnAction(e -> window.setScene(mainMenu));
			MenuButton fileButton = new MenuButton("File");
			fileButton.setOnAction(e -> {
				timer.stop();
			});
			
			MenuItem newArena = new MenuItem("New");
			TextInputDialog ArenaSizeX = new TextInputDialog("");  //show prompt requesting arenas x length
			ArenaSizeX.setContentText("Enter Arena's X length");
			TextInputDialog ArenaSizeY = new TextInputDialog(""); //show prompt requesting areans y length
			ArenaSizeY.setContentText("Enter Arena's Y length");
			newArena.setOnAction(e ->{
				ArenaSizeX.showAndWait();
				newArenaSizeX = Integer.parseInt(ArenaSizeX.getEditor().getText());
				ArenaSizeY.showAndWait();
				newArenaSizeY = Integer.parseInt(ArenaSizeY.getEditor().getText());
			
				d = new DroneArena(newArenaSizeX, newArenaSizeY);
				addDrone.setVisible(true);
				createArena.setVisible(false);
				pauseAnimation.setVisible(true);
				startAnimation.setVisible(true);
			});
			
			MenuItem save = new MenuItem("Save");
			FileChooser fileChooser = new FileChooser();
			save.setOnAction(e ->{
	            //Set extension filter for text files
	            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
	            fileChooser.getExtensionFilters().add(extFilter);
	            //Show save file dialog
	            File file = fileChooser.showSaveDialog(primaryStage);
	            
	            String arenaSize = String.valueOf(newArenaSizeX) + "," + String.valueOf(newArenaSizeY); //Write the arena size on the first line
	            PrintWriter clearText;
				try {
					clearText = new PrintWriter(file);
					clearText.print("");
		        	clearText.close();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        	
	            saveTextToFile(arenaSize, file);
	            if (file != null) {
	            
					for (String drone : DroneArena.drones) { //Write each drone in the arraylist on a new line
						saveTextToFile(drone, file);
					}
	            }
			});
	           
			MenuItem load = new MenuItem("Load");
			
			load.setOnAction(e ->{
				//load code here
				File file = fileChooser.showOpenDialog(primaryStage);
                if(file != null){
                	readFile(file);
                	printDrone();
                	droneInfo.setVisible(true);
                	addDrone.setVisible(true);
    				pauseAnimation.setVisible(true);
    				startAnimation.setVisible(true);
    				createArena.setVisible(false);
                }
			});
			fileButton.getItems().addAll(newArena, save, load);
			toolBar.getItems().add(fileButton);
			toolBar.getItems().add(helpButton);
			
			addDrone = new Button("Add Drone");
			addDrone.setOnAction(e -> {
				d.addDrone();
				printDrone();
				droneInfo.setText(d.toString());
				droneInfo.setVisible(true);
				
			});
			
			startAnimation = new Button("Start");
			startAnimation.setOnAction((event) -> {
				droneInfo.setText(toString());
				pausePressed = false;
				timer.start();
			});
			
			pauseAnimation = new Button("Pause");
			pauseAnimation.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> { 
				pausePressed = true;
				timer.stop();
			});
			//the 3 buttons visibility below should be false because it should only been shown when a new arena has been created or loaded
			addDrone.setVisible(false);
			pauseAnimation.setVisible(false);
			startAnimation.setVisible(false);
			
			addDrone.setLayoutX(50);
			addDrone.setLayoutY(800);
			
			startAnimation.setLayoutX(135);
			startAnimation.setLayoutY(800);
			pauseAnimation.setLayoutX(185);
			pauseAnimation.setLayoutY(800);
			
			//layout2 aka DroneArena
			layout2.getChildren().addAll(createArena, droneInfo, addDrone, startAnimation, pauseAnimation, toolBar);
			droneArena = new Scene(layout2, 1000, 1000);
			
			window.setScene(droneArena); //change back to main menu
			window.setTitle("28016965 Drone Simulator");
			window.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * This method saves a string on a new line to a defined file.
	 * @param content | The data to be added to the file
	 * @param file | an instance of file, instantiated above in the start animation button eventhandler in the start method.
	 */
	
	private void saveTextToFile(String content, File file) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(new FileWriter(file, true));
            writer.println(content);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(DroneInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	/**
	 * This method reads the text in the defined file and manages it accordingly. 
	 * For example, the first line is the arenas size and every line after is a drone. 
	 * Thus, the first line should be used to create a new DroneArena and every line after should be added to the drones list.
	 * @param file | an instance of file, instantiated above in the start animation button eventhandler in the start method.
	 */
	public static void readFile(File file){
	
        
        FileReader fr;
		try {
			fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);//read 'file'
		
			String firstLine = br.readLine();
	
			for (String drone = br.readLine(); drone != null; drone = br.readLine()) {
			
				DroneArena.drones.add(drone);
			}
			
			String[] firstParts = firstLine.split(",");
			String part1 = firstParts[0]; //x
			
			String part2 = firstParts[1]; //y
			
			int arenaSizeX = Integer.parseInt(part1); //convert the part before the comma to an int
			int arenaSizeY = Integer.parseInt(part2); //convert the part after the comma to an int
			d = new DroneArena(arenaSizeX, arenaSizeY); // create arena using the text files data
			
			br.close();
			
		}
 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static CopyOnWriteArrayList<Circle> circles = new CopyOnWriteArrayList<Circle>();
	/**
	 * This method will print the drone at a new x and y coordinate.
	 * This is achieved by iterating through the list of drones and creating a circle at that location.
	 * This is where the "drone drag" effect problem is.
	 */
	public static void printDrone(){ 
		for (String drone : DroneArena.drones) {
			String id = String.valueOf(DroneArena.drones.indexOf(drone));
			Circle circle = new Circle();
			String[] coords = drone.split(",");
			int accX = Integer.parseInt(coords[0]);//the part before the first comma
			int accY = Integer.parseInt(coords[1]);//the part after the first comma / before the second comma
			circle.setCenterX(accX); 
			circle.setCenterY(accY); 
			circle.setRadius(10.0f);
			circle.setId(id);
			circles.add(circle);
			layout2.getChildren().add(circle); //add the circle to the screen so it is visible to the user.
		}
	}
	
	public static void main(String[] args) {
		launch(args);
		
	}
}
