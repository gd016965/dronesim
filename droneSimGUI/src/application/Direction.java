package application;

import java.util.Random;
/**
 * 
 * @author JordanSavage
 *	This enum contains the enum's and their respective value
 */
public enum Direction {
	NORTH("NORTH"), EAST("EAST"), SOUTH("SOUTH"), WEST("WEST");

	public final String value;

	static Direction currentDirection;
	static Direction nextDirection;

	Direction(String s) {
		this.value = s;
	}
	/**
	 * Randomly select a direction
	 * @return the string value of the direction
	 */
	private static Direction getRandom() {
		
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}
	/**
	 * Use the getRandom method to choose a direction.
	 * @return the current direction. This is a string value of the enum selection.
	 */
	public static Direction getCurrentDirection() {
		currentDirection = getRandom();
		return currentDirection;
	}
	/**
	 * Same as the getCurrentDirection method.
	 * @return the next direction. This is a string value of the enum selection.
	 */
	public static Direction nextDirection() {
		nextDirection = getRandom();
		//ensure the drones next direction is not the same as the previous direction
		if (nextDirection.value == currentDirection.value) {
			nextDirection(); 

		}
		return nextDirection;
	}

	public static void main(String[] args) {
		getCurrentDirection();
		nextDirection();
		//System.out.println("Initial direction is " + currentDirection + ", next direction is " + nextDirection);
	}
}
