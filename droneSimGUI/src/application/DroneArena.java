package application;
import java.util.Random; //import the random module
import java.io.Serializable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.stage.Stage;

public class DroneArena extends Application implements Serializable {

	private static final long serialVersionUID = 6478342463652727456L;
	static int x; // initialisation of x coordinate
	static int y; // initialisation of y coordinate
	static char[][] Arena; // initialisation of 2D array, i.e. Arena
	public static int xRan; // initialisation of random x coordinate
	public static int yRan; // initialisation of random y coordinate
	public static CopyOnWriteArrayList<String> drones = new CopyOnWriteArrayList<String>();
	static int i = 0; // ArrayList index pointer initialisation
	static int id = 0;
	static Direction direction;
	static Direction updatedDirection;
	static Drone d = new Drone(x, y, direction);
	static int accX;
	static int accY;
	public static Direction accDir;

	public static String str;
	static int xCoord;
	static int yCoord;

	static String[] coordsList;
	public static String strOfCurrentDirection;
	
	/**
	 * 
	 * @param xLen | This is the x length of the arena
	 * @param yLen | This is the y length of the arena
	 */
	public DroneArena(int xLen, int yLen) {

		x = xLen;
		y = yLen;

		Arena = new char[xLen][yLen];
	}
	/**
	 * This method generates random coordinates 
	 * @return the coordinates in a string in an x,y format.
	 */
	private static String genRandomCoords() {
		String coords = "";
		Random randomGenerator;
		randomGenerator = new Random();
		xRan = randomGenerator.nextInt(x) + 1; // +1 to ensure drone is not assigned border position
		coords += xRan + ",";
		randomGenerator = new Random();
		yRan = randomGenerator.nextInt(y) + 1; // +1 to ensure drone is not assigned border position
		coords += yRan;

		return coords;

	}
	
	/**
	 * This method adds a drone, but first by checking if the drone can be added at the coordinates it has been given, 
	 * by using the method, isHere. If this method returns false, the add drone method is okay to add a drone.
	 */
	public void addDrone() {
		boolean isHereBool;
		if (drones.size() < (x * y)) {

			do {
				str = genRandomCoords();
				xCoord = Integer.parseInt(str.substring(0, str.indexOf(","))); //get x coord
				yCoord = Integer.parseInt(str.substring(str.indexOf(",") + 1, str.length())); //get y coord

				direction = Direction.getCurrentDirection();
				isHereBool = Drone.isHere(xCoord, yCoord);

			} while (isHereBool == true);

			drones.add(i, xCoord + "," + yCoord + "," + direction); 
			//System.out.println(drones);
			i++;

		} else {
			//arena is full
			//System.out.println("Arena full");
		}

	}
	
	
	public static Drone getDroneAt(int x, int y) {
		if (drones.contains(x + "," + y)) {
			//System.out.println("Drone at " + x + "," + y);
		}

		return null;
	}
	/**
	 * Iterate through the list of drones returning information about the location and direction of each drone.
	 */
	public String toString() {
		String listOfDrones = "";

		for (String drone : drones) {
			id = drones.indexOf(drone);
			String accDir = drone.split(",")[2]; // String value of direction from the element in the arrayList
			String[] coords = drone.split(",");
			accX = Integer.parseInt(coords[0]);
			accY = Integer.parseInt(coords[1]);
			listOfDrones += "Drone " + id + " is at (" + accX + ", " + accY + "), in the direction of " + accDir + "\n";

		}
		return listOfDrones;
	}

	public static void main(String[] args) {
		
	}
	/**
	 * 
	 * @param newX | the potential new x coordinate the drone can move to
	 * @param newY | the potential new y coordinate the drone can move to
	 * @return true or false depending on if the drone can move to that position (check if the drone is inside the arena)
	 */

	public static boolean canMoveHere(int newX, int newY) {

		if (newY < 1 || newY > y || newX < 1 || newX > x) { //check to see if position is inside or outside of the arena

			return false;

		}

		return true;

	}
	/**
	 * 
	 * @param id2 | drones id
	 * @param direction | drones direction
	 * @param newX | the next x coordinate the drone will move to (because it has passed the canMoveHere and isHere checks.
	 * @param newY | the next y coordinate ''
	 */
	public static void updateDrone(int id2, Direction direction, int newX, int newY) {
		//updates the drone by removing and reading it with new values. Could use .set() instead?
		updatedDirection = direction;
		DroneArena.direction = updatedDirection;
		drones.remove(id2); //remove the out-dated drone
		drones.add(id2, newX + "," + newY + "," + updatedDirection); //replace the out-dated with the up-to-date drone
	}
	
	/**
	 * Iterate the list of drones attempting to move each at a time. 
	 */
	public static void moveAllDrones() {
		
		Direction currentDirection;

		for (String drone : drones) {
			id = drones.indexOf(drone);
			String[] coords = drone.split(",");
			accX = Integer.parseInt(coords[0]);// get x coordinate
			accY = Integer.parseInt(coords[1]);// get y coordinate

			String droneWithCurrentID = drones.get(id); // direction in string
			strOfCurrentDirection = droneWithCurrentID.split(",")[2];
			currentDirection = Direction.valueOf(strOfCurrentDirection); // convert string to direction

			Drone.tryToMove(id, currentDirection, accX, accY);
			//System.out.println("Drone " + id + " is at (" + accX + ", " + accY + "), in the direction of "
			//		+ strOfCurrentDirection + "\n");
		}
		
		DroneInterface.printDrone(); 

	}
	

	public static void wait2Seconds() {
		try {
			TimeUnit.SECONDS.sleep(1); //sleep for 200ms as requested
			//DroneInterface.doDisplay();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public static void moveAllDronesTenTimes() {
		for (int i = 0; i <= 10; i++) {
			moveAllDrones();
			wait2Seconds();
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
